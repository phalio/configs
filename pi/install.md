# Install

## Image

https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/

sudo dd if=2022-09-22-raspios-bullseye-arm64-lite.img of=/dev/sd? bs=1M status=progress conv=fsync
sudo growpart /dev/sd? 2
sudo e2fsck -f /dev/sd?2
sudo resize2fs /dev/sd?2

touch /media/lily/boot/ssh

(not working for some reason, manual config required (or just my bugged router?))
echo 'mypassword' | openssl passwd -6 -stdin
echo "lily:encrypted-password" > /media/lily/boot/userconf

## Packages

### apt

```
sudo apt install wireguard-tools tmux emacs git build-essential rsync syncthing htop wget neofetch man ffmpeg hunspell hunspell-en-gb hunspell-en-us hunspell-de-de python3-pip jq lynx youtube-dl xclip sqlite3 postgresql libpq5 libpq-dev wakeonlan speedtest-cli ddclient autoconf nginx mumble-server
```

### snap

```
sudo snap install hugo
```

### pip

```
pip3 install 
```

## git

```
git config --global user.name "lily"
git config --global user.email "alilybit@disroot.org"
```

### Other



## Config

- run install scripts for general and device
- postgresql dir
- docker dir

## Server Stuff

- ddclient
- nginx
- certbot
- hugo
- murmur
- synapse/dendrite/conduit
- turn
- docker
- syncthing
- snowflake
- lilkey

## Back Up Fresh Install
