#!/bin/sh

# monitors
kscreen-doctor output.DP-1.mode.0 output.DP-1.position.0,450 output.DP-1.priority.3 output.DP-3.mode.0 output.DP-3.position.1920,0 output.DP-3.priority.1 output.DP-2.mode.0 output.DP-2.position.3840,280 output.DP-2.priority.2 output.DP-2.scale.2

# directories
mkdir ~/mnt
mkdir ~/mnt/data
mkdir ~/mnt/fideos1
mkdir ~/mnt/fideos2
mkdir ~/mnt/fideos3
mkdir ~/mnt/windows
mkdir ~/mnt/windata
mkdir ~/mnt/backup
mkdir ~/mnt/tatl
mkdir ~/mnt/phai

# symlink directories
ln -s /home/lily/mnt/data/documents /home/lily/
ln -s /home/lily/mnt/data/text /home/lily/
ln -s /home/lily/mnt/data/code /home/lily/
ln -s /home/lily/mnt/data/downloads /home/lily/
ln -s /home/lily/mnt/data/fideos /home/lily/
ln -s /home/lily/mnt/data/images /home/lily/
ln -s /home/lily/mnt/data/images /home/lily/
ln -s /home/lily/mnt/data/audio /home/lily/
ln -s /home/lily/mnt/data/other /home/lily/
ln -s /home/lily/mnt/data/tmp /home/lily/
ln -s /home/lily/mnt/data/vms /home/lily/
ln -s /home/lily/mnt/fideos3/downloads /home/lily/fideos/
ln -s /home/lily/mnt/data/downloads /home/lily/Downloads/_downloads
ln -s /home/lily/mnt/data/audio /home/lily/Music/_audio
ln -s /home/lily/mnt/data/images /home/lily/Pictures/_images
ln -s /home/lily/mnt/fideos2/editing /home/lily/fideos/editing-archive

# copy config files
cp -rv home/. $HOME/
sudo cp -rv etc/. /etc/
sudo cp -rv usr/. /usr/
sudo cp -rv boot/. /boot/

# activate systemd services
sudo systemctl enable --now ufw.service
sudo systemctl enable --now sddm.service
sudo systemctl enable --now NetworkManager
sudo systemctl enable --now gpm
sudo systemctl enable --now cronie
systemctl --user enable --now pipewire-pulse
sudo systemctl enable --now nvidia-suspend.service
sudo systemctl enable --now nvidia-hibernate.service
sudo systemctl enable --now nvidia-resume.service
sudo systemctl enable --now irqbalance
sudo systemctl enable --now wg-quick@wg0
sudo systemctl enable --now wg-quick@ch12
sudo systemctl enable --now sshd
sudo systemctl enable --now syncthing@lily.service
sudo systemctl enable --now kbct
sudo systemctl enable --now primary-monitor-after-suspend.service

# keyboard layouts
cd $HOME/code/git/ppkb-layouts/
sudo ./install-xkb.sh
sudo ./install-kbd.sh

# fixes
sudo ln -s /usr/bin/ksshaskpass /usr/lib/ssh/ssh-askpass

# bin
sudo ln -s /home/lily/code/ani-cli/ani-cli /usr/local/bin/
sudo ln -s /home/lily/code/mpv-save-bookmarks/mpvs /usr/local/bin/
sudo ln -s /home/lily/code/ChristBASHTree/tree-EN.sh /usr/local/bin/christbashtree
sudo ln -s /home/lily/code/famitracker-bpm/ft-bpm.py /usr/local/bin/
sudo ln -s /home/lily/code/khinsider/khinsider.py /usr/local/bin/
sudo ln -s /home/lily/code/twitch-nunc/twitch-nunc /usr/local/bin/
