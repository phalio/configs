# packages

## pacman

sudo pacman -S base linux linux-firmware mkinitcpio man-db man-pages texinfo linux-headers intel-ucode base-devel neovim nano networkmanager wpa_supplicant netctl xorg nvidia nvidia-settings nvidia-utils lib32-nvidia-utils ufw plasma plasma-wayland-session sddm flatpak syncthing htop neofetch wl-clipboard firefox bemenu-wayland dolphin dolphin-plugins ark foot imv emacs tmux wireguard-tools rsync git mpv streamlink cmus wget xorg-xev pavucontrol tmatrix veracrypt python-pip python-pipx cronie playerctl jq lynx youtube-dl yt-dlp genact blueman noto-fonts-cjk noto-fonts-emoji noto-fonts otf-ipafont wol mosh perl-image-exiftool cloud-guest-utils mpv-mpris qt6-wayland spectacle virtualbox virtualbox-host-modules-arch ksysguard virtualbox-guest-iso ffmpegthumbs kdegraphics-thumbnailers  kimageformats libheif qt5-imageformats taglib kgpg kdf kmix partitionmanager print-manager skanpage filelight kate gpm hwinfo steam steam-native-runtime wine whois ispell hunspell hunspell-en_gb hunspell-en_us hunspell-de ntfs-3g lib32-systemd gamemode lib32-gamemode pipewire-pulse lib32-sdl2 irqbalance element-desktop wine wine-gecko wine-mono winetricks maliit-keyboard sshfs alsa-utils strace lutris gst-libav quodlibet evtest fcitx5-im fcitx5-mozc aarch64-linux-gnu-gcc exfatprogs arch-install-scripts xf86-input-wacom kcm-wacom-tablet python-colorama perl-rename dnsutils chafa gomuks ddrescue multipath-tools python-pyusb qt6-imageformats opentoonz unrar-free pacman-contrib

## flatpak

flatpak install flathub com.github.Eloston.UngoogledChromium com.github.micahflee.torbrowser-launcher info.mumble.Mumble org.mozilla.Thunderbird com.ulduzsoft.Birdtray org.ferdium.Ferdium org.kde.ktorrent org.qutebrowser.qutebrowser chat.revolt.RevoltDesktop com.github.vladimiry.ElectronMail org.kde.krita org.gimp.GIMP org.inkscape.Inkscape org.tenacityaudio.Tenacity io.lmms.LMMS org.famistudio.FamiStudio io.github.quodlibet.ExFalso org.kde.kdenlive fr.natron.Natron org.blender.Blender com.vscodium.codium org.skytemple.SkyTemple org.libreoffice.LibreOffice org.videolan.VLC io.mpv.Mpv org.atheme.audacious io.github.cmus.cmus com.spotify.Client io.github.hakuneko.HakuNeko de.mediathekview.MediathekView org.kde.gwenview com.calibre_ebook.calibre org.omegat.OmegaT com.valvesoftware.Steam net.minetest.Minetest org.DolphinEmu.dolphin-emu net.kuribo64.melonDS org.citra_emu.citra org.yuzu_emu.yuzu info.cemu.Cemu io.mgba.mGBA ca._0ldsk00l.Nestopia com.snes9x.Snes9x com.github.shonumi.gbe-plus com.github.Rosalie241.RMG org.keepassxc.KeePassXC org.speedcrunch.SpeedCrunch com.obsproject.Studio com.belmoussaoui.Decoder org.fontforge.FontForge org.kde.kiten org.kde.kleopatra org.kde.konversation org.kde.okular org.darktable.Darktable net.sapples.LiveCaptions org.ryujinx.Ryujinx org.tildearrow.furnace com.polyphone_soundfonts.polyphone

## aur

aseprite midieditor davinci-resolve-studio makemkv genymotion vgmtrans-git tmatrix mkinitcpio-numlock ttf-ms-win11-auto streamlink-twitch-gui-bin freetube-bin kbct-git emacs-mozc twitch-cli-bin steamguard-cli

## pip

git+https://github.com/wustho/epy obsws-python demjson3 cloudscraper

## wine

dn-famitracker nsfplay-synthesia fl-studio ahk

## manual

## stuff

### kde autostart

#### applications

fcitx5

#### login scripts

login-restart-emacs-service

### steam

uncomment the [multilib] lines in /etc/pacman.conf
upgrade
rm ~/.local/share/Steam/steamapps/common/Team Fortress 2/bin/libSDL2-2.0.so.0
ln -s /usr/lib32/libSDL2-2.0.so.0 "$HOME/.steam/root/ubuntu12_32/steam-runtime/pinned_libs_32/"

### resolve activation

temporarily chown files to user
sudo chown -R lily /opt/resolve
sudo chown -R root /opt/resolve

### special application launchers

MOZ_ENABLE_WAYLAND=1 flatpak run org.ferdium.Ferdium & flatpak run info.mumble.Mumble mumble://lilyb.it/Tree House?title=Friendship Forest&version=1.2.0 & element-desktop & flatpak run org.mozilla.Thunderbird & com.github.vladimiry.ElectronMail
wine /home/lily/applications/Dn-FamiTracker_v0501_x64_Release/Dn-FamiTracker.exe & flatpak run org.tenacityaudio.Tenacity & flatpak run io.lmms.LMMS
/usr/bin/vboxmanage startvm "Capitalism" & /opt/genymotion/player --vm-name "Samsung Galaxy S10"

### monitor setup

/usr/bin/kscreen-doctor output.DP-1.mode.0 output.DP-1.position.0,450 output.DP-1.priority.3 output.DP-3.mode.0 output.DP-3.position.1920,0 output.DP-3.priority.1 output.DP-2.mode.0 output.DP-2.position.3840,280 output.DP-2.priority.2 output.DP-2.scale.2

### gnome extensions

unite
hide top bar
quick close in overview
