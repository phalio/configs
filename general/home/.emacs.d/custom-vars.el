(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(highlight-indent-guides-auto-enabled nil)
 '(highlight-indent-guides-method (quote bitmap))
 '(package-selected-packages
   (quote
    (lice highlight-indent-guides highligh-indent-guides highlight-indentation sml-mode systemd logview apache-mode systemd-mode docker-compose-mode crontab-mode nginx-mode fish-mode python-mode cmake-mode dockerfile-mode typescript-mode php-mode go-mode rust-mode json-mode git-modes hungry-delete move-text yaml-mode doom-modeline smart-mode-line anzu lsp-mode vscode-dark-plus-theme xclip wcheck-mode wc-mode wc-goal-mode use-package line-reminder))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#222222" :foreground "#dddddd" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal :foundry "default" :family "default"))))
 '(line-number ((t (:inherit default :background "#333333" :foreground "#888888"))))
 '(vertical-border ((t (:foreground "#888888")))))
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
