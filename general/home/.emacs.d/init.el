;; Move customization variables to a separate file and load it
(setq custom-file (locate-user-emacs-file "custom-vars.el"))(load custom-file 'noerror 'nomessage)

;;-----------------------------------------------------------------------------

;; COMMANDS TO RUN ONCE FOR FIRST-TIME INSTALLATION

;all-the-icons-install-fonts
;M-x package-install RET use-package RET
;package-refresh-contents

;;-----------------------------------------------------------------------------

;; INCLUDED MINOR MODES

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(toggle-frame-maximized)

;; pinephone bad performance
(when (string= (system-name) "midna")
  (global-display-line-numbers-mode 1)
  (custom-set-faces
   '(line-number ((t (:inherit default :background "#333333" :foreground "#888888"))))))
(when (string= (system-name) "midna")
  (defun on-frame-open (frame)
    (if (not (display-graphic-p frame))
	(custom-set-faces
	 '(line-number ((t (:inherit default :background "unspecified-bg" :foreground "#888888")))))))
  (on-frame-open (selected-frame))
  (add-hook 'after-make-frame-functions 'on-frame-open)
  (defun on-after-init ()
    (unless (display-graphic-p (selected-frame))
      (custom-set-faces
       '(line-number ((t (:inherit default :background "unspecified-bg" :foreground "#888888")))) (selected-frame))))
  (add-hook 'window-setup-hook 'on-after-init))

(xterm-mouse-mode 1)
(save-place-mode 1)
(savehist-mode 1)
(cua-mode 1)
(recentf-mode 1)
;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; pinephone bad performance
(when (string= (system-name) "midna")
  (global-hl-line-mode 1))
(when (string= (system-name) "midna")
  (defun on-frame-open (frame)
    (if (not (display-graphic-p frame))
	(set-face-background 'hl-line "unspecified-bg")))
  (on-frame-open (selected-frame))
  (add-hook 'after-make-frame-functions 'on-frame-open)
  (defun on-after-init ()
    (unless (display-graphic-p (selected-frame))
      (set-face-background 'hl-line "unspecified-bg" (selected-frame))))
  (add-hook 'window-setup-hook 'on-after-init))

;(tab-line-mode 1)
(ido-mode 1)
;; line word wrap
(global-visual-line-mode 1)
;; make autosave use regular save instead of saving temporary versions (best done in combination with versioning to still be able to undo edits)
(auto-save-visited-mode 1)
;; don't create backup files (the ones with ~)
(setq make-backup-files nil)
;; automatically activate spell checking in text major modes
(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

;;-----------------------------------------------------------------------------

;; THEME

(set-face-attribute 'default nil :height 110
		                 :font "Hack")
(setq-default scroll-bar-width 10)

;;; give gui a background color but not terminals for their built-in transparency background
  (defun on-frame-open (frame)
    (if (not (display-graphic-p frame))
	(set-face-background 'default "unspecified-bg" frame)))
  (on-frame-open (selected-frame))
  (add-hook 'after-make-frame-functions 'on-frame-open)
  (defun on-after-init ()
    (unless (display-graphic-p (selected-frame))
      (set-face-background 'default "unspecified-bg" (selected-frame))))
  (add-hook 'window-setup-hook 'on-after-init)

;; vertical split border
(set-display-table-slot standard-display-table
                        'vertical-border 
                        (make-glyph-code ?│))

;;-----------------------------------------------------------------------------

;; KEY BINDINGS

(global-set-key (kbd "C-f") 'isearch-forward)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)
(global-set-key (kbd "C-a") 'mark-whole-buffer)
(global-set-key (kbd "C-x f") 'occur)
(global-set-key (kbd "C-p") 'rgrep)
(global-set-key (kbd "<select>") 'end-of-visual-line)

;;; move-beginning-of-line-or-indentation
(defun at-or-before-indentation-p ()
  (save-excursion
    (let ((old-point (point)))
      (back-to-indentation)
      (<= old-point (point)))))
(defun move-beginning-of-line-or-indentation () (interactive)
       "If at the begining of line go to previous line.
 If at the indention go to begining of line.
 Go to indention otherwise."
       (cond ((bolp) (forward-line -1))
             ((at-or-before-indentation-p) (move-beginning-of-line nil))
             (t (back-to-indentation))))
(global-set-key (kbd "<home>") 'move-beginning-of-line-or-indentation)

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
		    ((numberp (cdr alpha)) (cdr alpha))
		    ;; Also handle undocumented (<active> <inactive>) form.
		    ((numberp (cadr alpha)) (cadr alpha)))
	      100)
	 '(90 . 90) '(100 . 100)))))
(global-set-key (kbd "C-c o") 'toggle-transparency)
(global-set-key (kbd "C-t") 'ido-switch-buffer)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-<tab>") 'tab-to-tab-stop)

(defun my-delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point))))

(defun my-backward-delete-word (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (my-delete-word (- arg)))

(defun my-delete-line ()
  "Delete text from current position to end of line char.
This command does not push text to `kill-ring'."
  (interactive)
  (delete-region
   (point)
   (progn (end-of-line 1) (point))))

(defun my-delete-line-backward ()
  "Delete text between the beginning of the line to the cursor position.
This command does not push text to `kill-ring'."
  (interactive)
  (let (p1 p2)
    (setq p1 (point))
    (beginning-of-line 1)
    (setq p2 (point))
    (delete-region p1 p2)))

(defun delete-current-line ()
 "Deletes the current line"
 (interactive)
 (forward-line 0)
 (delete-char (- (line-end-position) (point)))
 (delete-backward-char 1))

(defun aza-delete-line ()
  "Delete from current position to end of line without pushing to `kill-ring'."
  (interactive)
  (delete-region (point) (line-end-position)))

(global-set-key (kbd "C-S-k") 'my-delete-line) ; Ctrl+Shift+k
(global-set-key (kbd "C-k") 'delete-current-line)
(global-set-key (kbd "<C-delete>") 'my-delete-word)
(global-set-key (kbd "<C-backspace>") 'my-backward-delete-word)

;;; insert date
(defun insert-date ()
  "Insert today's date at point"
  (interactive "*")
  (insert (format-time-string "%F")))
(global-set-key (kbd "C-c C-,") 'insert-date)

;;; insert time
(defun insert-time ()
  "Insert today's time at point"
  (interactive "*")
  (insert (format-time-string "%T")))
(global-set-key (kbd "C-c C-.") 'insert-time)


;; open common files
(defun open-todo ()
  (interactive)
  (find-file (format-time-string "~/text/notes/todo.org"))
  (insert ""))
(global-set-key (kbd "C-w") 'open-todo)

;; open directories
(global-set-key (kbd "C-x C-w") 'ido-find-file)
(global-set-key (kbd "C-e")  (lambda () (interactive)
                                     (cd "~/text/notes/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d t")  (lambda () (interactive)
                                     (cd "~/text/tech/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d d")  (lambda () (interactive)
                                     (cd "~/text/diary/2022/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d n")  (lambda () (interactive)
                                     (cd "~/text/dreams/2022/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d h")  (lambda () (interactive)
                                     (cd "~/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d 2")  (lambda () (interactive)
                                     (cd "~/text/2fideos/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d c")  (lambda () (interactive)
                                     (cd "~/text/8bit/")
                                     (call-interactively 'ido-find-file)))
(global-set-key (kbd "C-c C-d o")  (lambda () (interactive)
                                     (cd "~/text/characters/")
                                     (call-interactively 'ido-find-file)))

;;-----------------------------------------------------------------------------

;;STUFF

;; fix gui version not covering the entire area because it doesn’t use space that is smaller than a full character
(setq inhibit-startup-message t)
(setq frame-resize-pixelwise t)
;; automatically focus special windows like help and occur
(add-hook 'occur-hook (lambda () (pop-to-buffer occur-buf)))
(add-hook 'grep-mode-hook (lambda () (pop-to-buffer (get-buffer "*grep*"))))
(setq help-window-select t)
(add-hook 'compilation-mode-hook (lambda () (pop-to-buffer (get-buffer "*compilation*"))))
(setq global-auto-revert-non-file-buffers t)
(set-frame-parameter (selected-frame) 'alpha '(90 90))
;(set-frame-parameter (selected-frame) 'left-fringe '(50))
(fringe-mode '(0 . 0))
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; display images
(setq org-startup-with-inline-images t)

;; allow sudo + ssh
(set-default 'tramp-default-proxies-alist (quote ((".*" "\\`root\\'" "/ssh:%h:"))))

;; wayland clipboard
(when (string= (system-name) "tatl")
  (setq wl-copy-process nil)
  (defun wl-copy (text)
    (setq wl-copy-process (make-process :name "wl-copy"
                                        :buffer nil
                                        :command '("wl-copy" "-f" "-n")
                                        :connection-type 'pipe))
    (process-send-string wl-copy-process text)
    (process-send-eof wl-copy-process))
  (defun wl-paste ()
    (if (and wl-copy-process (process-live-p wl-copy-process))
	nil ; should return nil if we're the current paste owner
      (shell-command-to-string "wl-paste -n | tr -d \r")))
  (setq interprogram-cut-function 'wl-copy)
  (setq interprogram-paste-function 'wl-paste))

(when (string= (system-name) "midna")
  (setq wl-copy-process nil)
  (defun wl-copy (text)
    (setq wl-copy-process (make-process :name "wl-copy"
                                        :buffer nil
                                        :command '("wl-copy" "-f" "-n")
                                        :connection-type 'pipe))
    (process-send-string wl-copy-process text)
    (process-send-eof wl-copy-process))
  (defun wl-paste ()
    (if (and wl-copy-process (process-live-p wl-copy-process))
	nil ; should return nil if we're the current paste owner
      (shell-command-to-string "wl-paste -n | tr -d \r")))
  (setq interprogram-cut-function 'wl-copy)
  (setq interprogram-paste-function 'wl-paste))

;; flyspell multi-language
(with-eval-after-load "ispell"
  ;; Configure `LANG`, otherwise ispell.el cannot find a 'default
  ;; dictionary' even though multiple dictionaries will be configured
  ;; in next line.
  (setenv "LANG" "en_US.UTF-8")
  (setq ispell-program-name "hunspell")
  ;; Configure German, Swiss German, and two variants of English.
  (setq ispell-dictionary "en_GB,en_US,de_DE")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_GB,en_US,de_DE")
  ;; For saving words to the personal dictionary, don't infer it from
  ;; the locale, otherwise it would save to ~/.hunspell_de_DE.
  (setq ispell-personal-dictionary "~/.hunspell_personal"))
;; The personal dictionary file has to exist, otherwise hunspell will
;; silently not use it.
;(unless (file-exists-p ispell-personal-dictionary)
;  (write-region "" nil ispell-personal-dictionary nil 0))

;;-----------------------------------------------------------------------------

;; GUI DAEMON FIXES

(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (lambda (frame)
		(with-selected-frame frame
		  (setq line-spacing 0)
		  ;(set-face-background 'default "#222222")
		  (set-frame-parameter (selected-frame) 'alpha '(90 90))
		  (set-face-attribute 'default nil :height 110
				      :font "Hack")))))

;;-----------------------------------------------------------------------------

;; PACKAGE MANAGER

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.org/packages/") t)
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
(package-initialize)
;(package-refresh-contents)

;(package-install 'use-package)
(require 'use-package)

;;-----------------------------------------------------------------------------

;; CUSTOM PACKAGES

;; org-mode
(use-package org
  :pin gnu)
(setq org-support-shift-select t)
(setq org-startup-truncated nil)

(setq org-directory "~/notes/")

;;; disable some org-mode bindings
(define-key org-mode-map (kbd "C-c C-d") nil)
(define-key org-mode-map (kbd "C-c C-f") nil)
(define-key org-mode-map (kbd "C-t") nil)

;;; inline code
(defun org-fontify-inline-src-block (limit)
  "Fontify inline source block."
  (when (re-search-forward org-babel-inline-src-block-regexp limit t)
    (add-text-properties
     (match-beginning 1) (match-end 0)
     '(font-lock-fontified t face (t (:foreground "#008ED1" :background "#1C1C1C"))))
    (org-remove-flyspell-overlays-in (match-beginning 0) (match-end 0))
    t))
'(org-fontify-drawers)
'(org-fontify-inline-src-block)

;;; disable additional code block indentation
(setq org-edit-src-content-indentation 0)

;; system clipboard integration
(use-package xclip
  :ensure t)
(xclip-mode 1)

;; mark changed and saved lines
(use-package line-reminder
  :ensure t)
(global-line-reminder-mode 1)

;; vscode dark theme
(use-package vscode-dark-plus-theme
  :ensure t
  :config
  (load-theme 'vscode-dark-plus t))

(use-package lsp-mode
  :ensure t)

;; color matching brackets
(use-package rainbow-delimiters
  :ensure t)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;;git
(use-package magit
  :ensure t)

;; smart parentheses
(use-package smartparens
  :ensure t)
(require 'smartparens-config)
(add-hook 'js-mode-hook #'smartparens-mode)
(smartparens-mode 1)

(use-package org-bullets
  :ensure t
  :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
(setq inhibit-compacting-font-caches t)

(use-package company
  :ensure t)
(global-company-mode 1)

(use-package ace-window
  :ensure t)
(global-set-key (kbd "M-o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

(use-package expand-region
  :ensure t)
(global-set-key (kbd "M-a") 'er/expand-region)
(defun er/add-text-mode-expansions ()
      (make-variable-buffer-local 'er/try-expand-list)
      (setq er/try-expand-list (append
                                er/try-expand-list
                                '(mark-paragraph
                                  mark-page))))

(use-package anzu
  :ensure t)
(global-anzu-mode 1)

;; pinephone bad performance
(when (string= (system-name) "midna")
  (use-package doom-modeline
    :ensure t
    :init (doom-modeline-mode 1)))
(use-package all-the-icons
  :if (display-graphic-p))

(use-package yaml-mode
  :ensure t)
(add-hook 'yaml-mode-hook
    '(lambda ()
       (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; move line or selection with M-arrow
(use-package move-text
  :ensure t)
(move-text-default-bindings)

(use-package highlight-indent-guides
  :ensure t
  :init (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))
(setq highlight-indent-guides-method 'character)
(setq highlight-indent-guides-auto-enabled nil)
(set-face-foreground 'highlight-indent-guides-character-face "#666666")

;; insert licenses
(use-package lice
  :ensure t)

(use-package git-modes
  :ensure t)

(use-package json-mode
  :ensure t)

(use-package rust-mode
  :ensure t)

(use-package go-mode
  :ensure t)

(use-package php-mode
  :ensure t)

(use-package typescript-mode
  :ensure t)

(use-package dockerfile-mode
  :ensure t)

(use-package cmake-mode
  :ensure t)

(use-package python-mode
  :ensure t)

(use-package fish-mode
  :ensure t)

(use-package nginx-mode
  :ensure t)

(use-package crontab-mode
  :ensure t)

(use-package go-mode
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(use-package systemd
  :ensure t)

(use-package apache-mode
  :ensure t)

(use-package logview
  :ensure t)

(use-package sml-mode
  :ensure t)

;; more configs
(load "~/.emacs.d/templates.el")
(put 'magit-clean 'disabled nil)
