#!/bin/sh

# copy config files
cp -rv home/. $HOME/
sudo cp -rv etc/. /etc/
sudo cp -rv usr/. /usr/
sudo cp -rv boot/. /boot/

# activate systemd services
systemctl --user enable --now emacs
systemctl --user enable --now ssh-agent

# 24-bit colour
/usr/bin/tic -x -o ~/.terminfo /etc/xterm-24bit.terminfo
/usr/bin/tic -x -o /root/.terminfo /etc/xterm-24bit.terminfo
