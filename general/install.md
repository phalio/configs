# Install

## git

```
git config --global user.name "lily"
git config --global user.email "alilybit@disroot.org"
```

## Config

- run install scripts for general
- emacs
  - M-x package-install RET use-package RET
  - stop service, start emacs -nw, close and start service
  - e `~/.emacs.d/elpa/vscode-dark-plus-theme-20220217.350/vscode-dark-plus-theme.el` edit main bg and fg
- ssh keys
- wireguard
- set up codeberg ssh keys

`/usr/bin/tic -x -o ~/.terminfo /etc/xterm-24bit.terminfo` as root

## Back Up Fresh Install

## todo

