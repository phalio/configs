# applications

firefox nvidia steam oculus epicgames obs notepad++ sharex vlc audacious paint.net tenacity mumble beatsabermodmanager .net vtf-edit

https://github.com/NeilJed/VTFLib/releases

# browser logins

youtube, twitch, discord

# registry

## task bar top (apparently they made the value uneditable?)

HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\StuckRects3
change 03 to 01

# games

## tf2

-novid -nojoy -nosteamcontroller -nohltv -particles 1 -precachefontchars

## downgrade beatsaber

https://steamcommunity.com/sharedfiles/filedetails/?id=1805934840

Win+R steam://open/console
download_depot 620980 620981 886973241045584398
C:\Program Files (x86)\Steam\steamapps\common\Beat Saber
back up beatsaberdata/customlevels, dlc, userdata, customavatars, customsabers, plugins
delete everything in folder
copy everything from C:\Program Files (x86)\Steam\steamapps\content\app_620980\depot_620981 to C:\Program Files (x86)\Steam\steamapps\common\Beat Saber
if old plugin folder for version exists, paste it and rename to Plugins
paste above files
optionally reinstall mods

# ark

sens: 0.179001
fov multiplier: 1.18

[GAME FOLDER]\ShooterGame\Saved\Config\WindowsNoEditor\GameUserSettings.ini:

LookLeftRightSensitivity=0.179001
LookUpDownSensitivity=0.255716
FOVMultiplier=1.18
[/Script/Engine.InputSettings]
bEnableMouseSmoothing=False

# overwatch

VIDEO
VIDEO
vsync off
triple buffering off
reduce buffering on
nvidia reflex enabled+boost

GRAPHICS QUALITY
high quality upsampling: default
texture quality: high
texture filtering quality: epic 16x
local fog detail: low
dynamic reflections: off
shadow detail: low
model detail: high
effects detail: low
lighting quality: low
antialias quality: medium - smaa low
refraction quality: low
screenshot quality: 1x
ambient occlusion: low
local reflections: off
damage fx: low

DETAILS
all on

SOUND
dolby atmos
headphones
sounds when enemy and teammate

GAMEPLAY
GENERAL
high precision mouse input

HUD
all booleans ON except snap death camera to killer
20% waypoint opacity

ACCESSIBILITY
GENERAL
camera shake: reduced
hud shake: off
