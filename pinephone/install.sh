#!/bin/sh

# copy config files
cp -rv home/. $HOME/
sudo cp -rv etc/. /etc/
sudo cp -rv usr/. /usr/
sudo cp -rv boot/. /boot/

# activate systemd services
sudo systemctl enable --now ppkbbat-d.timer
sudo systemctl enable --now kbdrate.service &
sudo systemctl enable --now cronie
sudo systemctl enable --now wireguard_reresolve-dns.timer

# ppkb-layouts
cd $HOME/code/git/ppkb-layouts/
sudo ./install-xkb.sh
sudo ./install-kbd.sh

# compile ppkb-tools
cd $HOME/code/git/pinephone-keyboard/
make

# disable mobile internet auto connect
nmcli connection modify o2 autoconnect no

# bin
sudo ln -s /home/lily/code/ani-cli/ani-cli /usr/local/bin/
sudo ln -s /home/lily/code/mpv-save-bookmarks/mpvs /usr/local/bin/
sudo ln -s /home/lily/code/ChristBASHTree/tree-EN.sh /usr/local/bin/christbashtree
sudo ln -s /home/lily/code/famitracker-bpm/ft-bpm.py /usr/local/bin/
sudo ln -s /home/lily/code/khinsider/khinsider.py /usr/local/bin/
sudo ln -s /home/lily/code/pinephone-keyboard/build/ppkb-i2c-charger-ctl /usr/local/bin/
sudo ln -s /home/lily/code/ppkbbat-tools/ppkbbat-d /usr/local/bin/
sudo ln -s /home/lily/code/ppkbbat-tools/ppkbbat-info /usr/local/bin/
sudo ln -s /home/lily/code/ppkbbat-tools/ppkbbat-notif /usr/local/bin/
sudo ln -s /home/lily/code/ppkbbat-tools/ppkbbat-setlimit /usr/local/bin/
sudo ln -s /home/lily/code/ppkbbat-tools/ppkbbat-toggle /usr/local/bin/
sudo ln -s /home/lily/code/twitch-nunc/twitch-nunc /usr/local/bin/
