# Install

## Image

(Possibly install squashfs-tools)

```
git clone https://github.com/dreemurrs-embedded/archarm-mobile-fde-installer.git
cd archarm-mobile-fde-installer/
./installer.sh
```

## Settings

- connect to network
- disable auto screen off
- timezone
- wifi priorities

`sudo systemctl enable --now sshd`

## Hostname & Username

`sudo hostnamectl hostname tatl`

[Username](https://lilyb.it/tech/sxmo#rename-user)

```
passwd
sudo passwd -l root
```

## Packages

### Pacman

`sudo pacman -Syyu`

```
sudo pacman -S --needed firefox wireguard-tools tmux emacs git base-devel rsync syncthing mpv streamlink cmus imv audacity itinerary keepassxc megapixels mumble htop audacious npm wget neofetch man ttf-hack whois ispell hunspell hunspell-en_gb hunspell-en_us hunspell-de socat php xorg-xev postprocessd qutebrowser pavucontrol python-pip cronie playerctl danctnix-tweaks-app usbutils android-tools speedcrunch jq lynx youtube-dl yt-dlp genact blueman thunderbird noto-fonts-cjk noto-fonts-emoji noto-fonts otf-ipafont wakeonlan mosh perl-image-exiftool python-psutil python-pipx mpv-mpris qt6-wayland fcitx5-im fcitx5-mozc dnsutils ffmpeg-v4l2-request chafa pacman-contrib debugedit pdfjs pacman-contrib
```

### yay

```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
yay -Y --gendb
yay -Syu --devel
yay -Y --devel --save
```

### AUR

```
yay -S gomuks-bin freetube-bin tmatrix-git swaylock-effects-git mepo ffmpeg-v4l2-request-git mpv-git
```

### pip

```
pipx install epy-reader speedtest-cli
```

## git

```
git config --global user.name "lily"
git config --global user.email "alilybit@disroot.org"
```

```
git clone git@codeberg.org:alilybit/configs.git;\
git clone git@codeberg.org:alilybit/ppkbbat-tools.git;\
git clone git@codeberg.org:alilybit/ppkb-layouts.git;\
git clone git@codeberg.org:alilybit/pinephone-utils.git;\
git clone git@codeberg.org:alilybit/twitch-nunc.git;\
git clone git@codeberg.org:alilybit/mpv-save-bookmarks.git;\
git clone https://megous.com/git/pinephone-keyboard;\
git clone https://aur.archlinux.org/yay.git;\
git clone https://github.com/sergiolepore/ChristBASHTree
```

### Other

```
wget ftp://ftp.invisible-island.net/ncurses-examples/ncurses-examples.tar.gz
tar -xvf ncurses-examples.tar.gz
cd ncurses-examples-*
./configure
make
sudo make install
```

- install custom kernel

## Config

- set up APN
- run install scripts for general and device
- emacs
  - M-x package-install RET use-package RET
  - stop service, start emacs -nw, close and start service
  - e `~/.emacs.d/elpa/vscode-dark-plus-theme-20220217.350/vscode-dark-plus-theme.el` edit main bg and fg
- ssh keys
- wireguard
- set up codeberg ssh keys

`/usr/bin/tic -x -o ~/.terminfo /etc/xterm-24bit.terminfo` as root

```
cd /boot
sudo ./mkscr
reboot
```

- use blueman-manager to connect earbuds
- configure firefox
- configure mumble
- log into websites and gomuks

### Firefox Config

#### about:config

toolkit.legacyUserProfileCustomizations.stylesheets = true

## Back Up Fresh Install
