#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# prompt
PS1='[\[\033[1;93m\]\u@\h\[\033[00m\]:\[\033[1;94m\]\W\[\033[00m\]]\$ '

# autocomplete symlinks
bind 'set mark-symlinked-directories on'

# ls colors
alias ls='ls --color=auto'

# auto complete with sudo
complete -cf sudo

## aliases
alias e="emacsclient -t -a emacs -nw"
alias ew="emacsclient -cn"
alias se="sudoedit"
alias sudo="sudo "
alias sudoc="TERM=xterm-256color sudo"
alias sshc="TERM=xterm-256color ssh"
alias 8b="TERM=xterm-256color"
alias pacman="pacman --disable-download-timeout"
alias wl-copy="WAYLAND_DISPLAY=wayland-1 wl-copy"
alias wl-paste="WAYLAND_DISPLAY=wayland-1 wl-paste"
alias timer="sxmo_timer.sh timerrun"
alias o="xdg-open"
alias lsl="ls -lah"
alias vu="sudo wg-quick up ch12"
alias vd="sudo wg-quick down ch12"

## env vars
export EDITOR="emacsclient -t -a emacs -nw"
export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/code/scripts:$HOME/code/scripts-priv:$HOME/code/ytdl:$HOME/code/ffmpeg:$HOME/code/pinephone-utils:$HOME/code/scripts-media:$HOME/code/server-scripts:$HOME/code/scripts-downloads"
export TERM="xterm-24bit"
export HISTCONTROL="ignoreboth"
export HISTIGNORE="wl-copy*"

# rust
#. "$HOME/.cargo/env"
