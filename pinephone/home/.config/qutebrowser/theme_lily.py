# allow transparency
c.window.transparent = True

# colours
blackm0 = "#111111"
red0 = "#bb3333"
green0 = "#22aa44"
yellowm1 = "#dd8833"
yellow0 = "#eebb33"
blue0 = "#2277cc"
purplem1 = "#442255"
purple0 = "#8844aa"
purple0_trans = "#e58844aa"
cyan0 = "#229999"
white0 = "#bbbbbb"
black1 = "#333333"
red1 = "#ee4444"
green1 = "#33cc66"
yellow1 = "#ffee33"
yellow1_trans = "#e5ffee33"
blue1 = "#3399dd"
purple1 = "#aa55dd"
cyan1 = "#33bbbb"
white1 = "#ffffff"
invis = "#00000000"

# elements
fg0 = "#dddddd"
bgm1 = "#111111"
bg0 = "#222222"
bg0_trans = "#e5222222"
bg1 = "#333333"
bg1_trans = "#e5333333"
tab_active_bg = "#888888"
tab_active_bg_trans = "#e5888888"
tab_inactive_bg = "#111111"
tab_inactive_bg_trans = "#e5111111"
tab_active_fg = "#222222"
tab_inactive_fg = "#888888"

# accents
accent = yellow1
url0 = green0
url1 = green1

# tabs
c.colors.tabs.bar.bg = invis
c.colors.tabs.selected.odd.fg = tab_active_fg
c.colors.tabs.selected.even.fg = tab_active_fg
c.colors.tabs.selected.even.bg = tab_active_bg_trans
c.colors.tabs.selected.odd.bg = tab_active_bg_trans
c.colors.tabs.odd.fg = tab_inactive_fg
c.colors.tabs.even.fg = tab_inactive_fg
c.colors.tabs.odd.bg = tab_inactive_bg_trans
c.colors.tabs.even.bg = tab_inactive_bg_trans

# status bar
c.colors.completion.fg = fg0
c.colors.statusbar.normal.fg = fg0
c.colors.statusbar.normal.bg = bg1
c.colors.statusbar.command.fg = fg0
c.colors.statusbar.command.bg = bg1
c.colors.statusbar.url.fg = url0
c.colors.statusbar.url.success.http.fg = url0
c.colors.statusbar.url.success.https.fg = url0
c.colors.statusbar.url.error.fg = url0
c.colors.statusbar.url.hover.fg = url1
c.colors.statusbar.url.warn.fg = url0
c.colors.statusbar.private.fg = fg0
c.colors.statusbar.private.bg = purplem1
c.colors.statusbar.command.private.fg = fg0
c.colors.statusbar.command.private.bg = purplem1

# menu
c.colors.completion.fg = fg0
c.colors.completion.odd.bg = bg0
c.colors.completion.even.bg = bg0
c.colors.completion.category.fg = accent
c.colors.completion.category.bg = bg1
c.colors.completion.category.border.top = invis
c.colors.completion.category.border.bottom = invis
c.colors.completion.item.selected.fg = bg0
c.colors.completion.item.selected.bg = accent
c.colors.completion.item.selected.border.top = invis
c.colors.completion.item.selected.border.bottom = invis
c.colors.completion.item.selected.match.fg = yellowm1
c.colors.completion.match.fg = yellow0
c.colors.completion.scrollbar.fg = tab_active_bg
c.colors.completion.scrollbar.bg = tab_inactive_bg

# context menu
c.colors.contextmenu.disabled.bg = tab_inactive_bg
c.colors.contextmenu.disabled.fg = tab_inactive_fg
c.colors.contextmenu.menu.bg = bg1
c.colors.contextmenu.menu.fg = fg0
c.colors.contextmenu.selected.bg = accent
c.colors.contextmenu.selected.fg = bg0

# download bar
c.colors.downloads.bar.bg = tab_inactive_bg
c.colors.downloads.start.fg = url0
c.colors.downloads.stop.fg = url0
c.colors.downloads.start.bg = tab_inactive_bg
c.colors.downloads.stop.bg = tab_inactive_bg
c.colors.downloads.error.fg = red0

# hints
c.colors.hints.fg = bg0
c.colors.hints.bg = yellow1_trans
c.colors.hints.match.fg = yellowm1

# info
c.colors.messages.error.fg = red0
c.colors.messages.error.bg = bg1
c.colors.messages.error.border = invis
c.colors.messages.warning.fg = red0
c.colors.messages.warning.bg = bg1
c.colors.messages.warning.border = invis
c.colors.messages.info.fg = fg0
c.colors.messages.info.bg = bg1
c.colors.messages.info.border = invis
c.colors.prompts.fg = fg0
c.colors.prompts.border = invis
c.colors.prompts.bg = bg1
c.colors.prompts.selected.bg = yellow1
c.colors.prompts.selected.fg = bg0

# progress bar
c.colors.statusbar.progress.bg = tab_active_bg
