config.load_autoconfig()

# default sites
c.url.start_pages = "https://search.disroot.org/"
c.url.searchengines = {
    "DEFAULT": "https://search.disroot.org/search?q={}",
    "d": "https://duckduckgo.com/?q={}",
    "g": "https://www.google.com/search?q={}",
    "a": "https://wiki.archlinux.org/?search={}",
    "w": "https://en.wikipedia.org/?search={}",
}

# font
c.fonts.default_family = "Hack"
c.fonts.default_size   = "9pt"

# visuals
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.darkmode.policy.page = "always"
c.colors.webpage.preferred_color_scheme = "dark"
c.colors.webpage.darkmode.policy.images = "never"
c.statusbar.show = "always"
c.tabs.show = "always"

# keyboard behaviour
c.input.insert_mode.auto_enter = False
c.input.insert_mode.auto_leave = False
c.input.insert_mode.plugins = False
c.input.forward_unbound_keys = "all"
ESC_BIND = "clear-keychain ;; search ;; fullscreen --leave"
c.bindings.default["normal"] = {}

# keyboard shortcuts
c.bindings.commands["normal"] = {
    "<ctrl+0>": "back",
    "<ctrl+1>": "forward",
    "<ctrl++>": "tab-prev",
    "<ctrl+backspace>": "tab-next",
    "<ctrl+e>": "tab-next",
    "<ctrl+w>": "scroll-px 0 -368",
    "<ctrl+s>": "scroll-px 0 368",
    "<ctrl+up>": "scroll-px 0 -1",
    "<ctrl+down>": "scroll-px 0 1",
    "<ctrl+return>": "cmd-set-text -s :open",
    "<ctrl+shift+return>": "cmd-set-text -s :open -t",
    "<ctrl+f>": "cmd-set-text /",
    "<ctrl+shift+f>": "fullscreen ;; config-cycle statusbar.show always never ;; config-cycle tabs.show always never",
    "<ctrl+/>": "hint",
    "<ctrl+shift+\>": "hint links tab",
    "<ctrl+,>": "hint all spawn --detach mpv {hint-url}",
    "<ctrl+shift+,>": "hint media spawn --detach mpv {hint-url}",
    "<ctrl+.>": "hint media download",
    "<ctrl+r>": "reload",
    "<ctrl+shift+r>": "config-source",
    "<ctrl+d>": "tab-clone",
    "<ctrl+m>": "config-cycle statusbar.show always never",
    "<ctrl+shift+m>": "config-cycle tabs.show always never",
    "<ctrl+q>": "tab-close",
    "<alt-x>": "cmd-set-text :",
    "<ctrl-t>": "cmd-set-text :tab-focus ",
    "<ctrl-g>": ESC_BIND,
    "<alt-w>": "search-prev",
    "<alt-s>": "search-next",
    "<ctrl-->": "zoom-out",
    "<ctrl-shift-_>": "zoom-in",
    "<ctrl+u>": "yank pretty-url",
    "<ctrl+shift+u>": "hint links yank",
    '1': 'fake-key 1',
    '2': 'fake-key 2',
    '3': 'fake-key 3',
    '4': 'fake-key 4',
    '5': 'fake-key 5',
    '6': 'fake-key 6',
    '7': 'fake-key 7',
    '8': 'fake-key 8',
    '9': 'fake-key 9',
    '0': 'fake-key 0',
}

# stuff
c.content.autoplay = False

# theme
c.window.transparent = True
config.source("theme_lily.py")
