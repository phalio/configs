#!/bin/sh
# configversion: 6363277a2c0b710bfa69218635da22d0
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

#sxmo_swaylock.sh

if [ -z "$SXMO_NO_MODEM" ]; then
	MMCLI="$(mmcli -m any -J 2>/dev/null)"
	if [ -z "$MMCLI" ]; then
		sxmo_notify_user.sh "Modem crashed! 30s recovery."
		sxmo_wakelock.sh lock sxmo_modem_crashed 30s
	fi
fi

# Add here whatever you want to do

# activate ppkb battery output if holds power and discharging
if [ "$(cat /sys/class/power_supply/ip5xxx-battery/status)" = "Discharging" ]; then
    sudo $HOME/code/pinephone-keyboard/build/ppkb-i2c-charger-ctl power-on
fi

# restart gomuks because it somehow takes up to 30 minutes for it to reconnect after suspend
tmux kill-session -t gomuks
# switch current workspace back to what it was before starting gomuks in workspace 3, but only if i was not on 3 before
if [ ! "$(swaymsg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)" = "3" ]; then
    switch_back=1
fi
swaymsg 'workspace number 3; exec tmux new -s gomuks -n lily -d gomuks && tmux new-window -t gomuks -n gibus -d $HOME/code/scripts-downloads/gm gibus & foot tmux a -t gomuks'
if [ "$switch_back" = "1" ]; then
    swaymsg 'workspace back_and_forth'
fi
