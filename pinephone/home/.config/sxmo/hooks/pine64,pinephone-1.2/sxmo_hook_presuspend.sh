#!/bin/sh

# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

$HOME/code/pinephone-utils/sxmo_swaylock.sh &

# deactivate ppkb battery output if holds power and discharging and pp battery is not low
if [ "$(cat /sys/class/power_supply/ip5xxx-battery/status)" = "Discharging" && "$(cat /sys/class/power_supply/axp20x-battery/capacity
)" -ge "25" ]; then
    sudo $HOME/code/pinephone-keyboard/build/ppkb-i2c-charger-ctl power-off
fi
