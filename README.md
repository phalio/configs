# configs

Configuration files I use on my devices along with commands, instructions and other stuff to set up new installs.

They are split up into `general` which applies to all (GNU/Linux) devices and other directories that apply only to certain devices. Each top directory has files with instructions, packages to install, commands to run and other stuff. Windows is included too but I only use Windows because I have to for some games, I’d be very happy if that were to change.

Currently included non-general configs:

- PinePhone 1.2 Arch Sxmo
- Desktop Arch KDE Plasma Wayland Nvidia
- Windows 11
